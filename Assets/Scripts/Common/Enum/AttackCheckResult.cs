﻿/// <summary>
/// Результат проверки возможности атаки
/// </summary>
public enum AttackCheckResult
{
    /// <summary>
    /// Можно атаковать
    /// </summary>
    Ok,
    /// <summary>
    /// Слишком большое расстояние до цели
    /// </summary>
    TooLongDistance,
    /// <summary>
    /// Оружие не готово к атаке
    /// </summary>
    WeaponIsBusy
}
