using System;
using UnityEngine;

/// <summary>
/// ��������� �������, �������� ����� ������� ����������� � ����������
/// </summary>
public interface IDamageable 
{
    /// <summary>
    /// ��������� �������
    /// </summary>
    Transform Transform { get; }

    /// <summary>
    /// ������� �������� HP �������
    /// </summary>
    int Health { get; }

    /// <summary>
    /// ������������ �������� HP �������
    /// </summary>
    int MaxHealth { get; }

    /// <summary>
    /// ���������� ��������� �����
    /// </summary>
    /// <param name="damageValue">�������� �����</param>
    void ReceiveDamage(int damageValue);

    /// <summary>
    /// �������� HP
    /// </summary>
    /// <param name="healthToAdd">���������� HP ��� ����������</param>
    void AddHealth(int healthToAdd);

    /// <summary>
    /// ������ ��� ��������� �����
    /// </summary>
    void HitEffect();

    /// <summary>
    /// ���������, ��� �� ������
    /// </summary>
    /// <returns></returns>
    bool IsAlive();

    /// <summary>
    /// ��������/��������� �����������
    /// </summary>
    /// <param name="enabled"></param>
    void SetEnabledRegeneration(bool enabled);

    /// <summary>
    /// �������� ��������� ������� ������ �������
    /// </summary>
    /// <param name="listener"></param>
    void RegisterKilledListener(Action listener);

    /// <summary>
    /// ������� ��������� ������� ������ �������
    /// </summary>
    /// <param name="listener"></param>
    void UnregisterKilledListener(Action listener);
}
