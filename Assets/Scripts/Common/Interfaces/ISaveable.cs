using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ��������� ����������, ���������� ��������� � ��������� ��� ���������
/// </summary>
public interface ISaveable
{
    /// <summary>
    /// ������ ��������� �������
    /// </summary>
    /// <param name="state">��������� �������</param>
    void SetState(object state);
    
    /// <summary>
    /// �������� ������� ��������� �������
    /// </summary>
    /// <returns></returns>
    object GetState();
}


