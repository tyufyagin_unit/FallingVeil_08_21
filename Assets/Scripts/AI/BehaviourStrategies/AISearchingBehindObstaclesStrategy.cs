using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// ��������� ��������, ����������� ��� ������ �������� �� ���� (������), 
/// ������� ����� �� ���� ������.
/// ��� ������������� ������� ��������� ���� ���� ������ �� ������ � �����, ��� � ��������� ��� 
/// ��� �����, �� � �� ��������� ������������
/// </summary>
public partial class AISearchingBehindObstaclesStrategy : AIMovementStrategy
{
    [SerializeField]
    [Tooltip("���� ��������, ������� ����� ��������� �� �������� �� �����, " +
        "�� ������� ���� � ��������� ��� �������� ����")]
    private float lastTargetPointRotationAngle = 60;

    [SerializeField]
    [Tooltip("����� �������� �� �����, ��� � ��������� ��� ���� �������� ����")]
    private float lastTargetPointRotationTime = 4;

    [SerializeField]
    [Tooltip("���� ��������, ������� ����� ��������� �� �������� �� ����� �� ������������, " +
        " ��� ��� ���������� �����")]
    private float behindObstacleRotationAngle = 60;

    [SerializeField]
    [Tooltip("����� �������� �� ����� �� ������������")]
    private float behindObstacleRotationTime = 4;

    [SerializeField]
    [Tooltip("���� ��������, ������� ����� ���������, ���� �� ����� �����������, " +
        "�� ������� ��� ���������� ����� ��� ���� ��� �������� � �����������" +
        "��� ����� ����������")]
    private float noObstacleRotationAngle = 360;

    [SerializeField]
    [Tooltip("������������ ����� �������� � ����� �� ������������")]
    private float maxObstacleSearchDuration = 5;

    /// <summary>
    /// ���� ������ �����
    /// </summary>
    private FieldOfView fov = null;

    /// <summary>
    /// ���� ������ (�����)
    /// </summary>
    private Transform target = null;

    /// <summary>
    /// ������� ��������� ����� �� ������������, ��� ���� ����� ������ ������
    /// (��������� ��� ���� ������ ��� �������)
    /// </summary>
    private ObstacleHidingPoint currentHidingPoint = null;

    /// <summary>
    /// ��������� �������� ���� � ����� �� ������������, ��� ���� ����� ������ ������
    /// (��������� ��� ���� ������ ��� �������)
    /// </summary>    
    private List<HidingPointPath> currentRetreatPathList = null;

    /// <summary>
    /// ������ ��� ������ ���������� ����������� ��� ������ ������
    /// </summary>
    private readonly System.Random random = new System.Random();

    /// <summary>
    /// ���������������� ������ ��������� ��������
    /// (������ ����� ���������� �������� ����� ������� StartMoving)
    /// </summary>
    /// <param name="movingObject">���������� ������ ��</param>
    /// <param name="fov">���� ������ �����</param>
    /// <param name="target">�����</param>
    public void Initialize(AIMovingObject movingObject, FieldOfView fov, Transform target)
    {
        BaseInitialize(movingObject);
        this.target = target;
        this.fov = fov;
    }

    /// <summary>
    /// ��������, ����������� �������� � �����, ��� � ��������� ��� ��� ������� �����,
    /// � � ������������
    /// </summary>
    /// <param name="coroutineTask"></param>
    /// <returns></returns>
    private IEnumerator PerformCheckingLastTargetPoint(CoroutineTask coroutineTask)
    {
        // ���� ����� ��������� �������� �� �����, ��� � ��������� ��� ��� ������� �����.
        movingObject.SetEnabledAutomaticRotation(false);

        Vector3 pointToLookAt = target.position;
        float angleToLastTargetPoint = VectorFunc.GetSignedAngleXZ(movingObject.transform.forward,
           pointToLookAt - movingObject.Position);

        Coroutine rotatingCoroutine = StartCoroutine(PerformInfiniteLookAt(pointToLookAt,
            movingObject.AlertRotationSpeed));

        // �������� � �����, �� ������� � ��������� ��� ��� ������� �����
        movingObject.MoveToPoint(target.position);

        yield return new WaitForFixedUpdate();

        while (!movingObject.IsDestinationReached())
        {
            yield return new WaitForFixedUpdate();
        }

        this.StopAndNullCoroutine(ref rotatingCoroutine);

        // ���������� ��������, �.�. ���� �������� ������ � ��������� �����, ��� ��� �����,
        // � ������ �� �������, ��� ������ ������.
        movingObject.SetIdleSpeed();

        // ����� ����, ��� ����� �� �����, ���� ��������� �������, ����� ���������,
        // ��� ���� ��� � ������ �����. ������� ��������� � �����������, � ������� ������� �����
        float rotationAngle = angleToLastTargetPoint >= 0 ? lastTargetPointRotationAngle 
            : -lastTargetPointRotationAngle;

        CoroutineTask rotationCoroutineTask = new CoroutineTask();
        rotationCoroutineTask.Start();
        StartCoroutine(PerformRotatingLeftRight(rotationAngle, movingObject.RotationSpeed, 
            lastTargetPointRotationTime, rotationCoroutineTask));
        
        while (rotationCoroutineTask.IsPerforming)
        {
            yield return null;
        }
        coroutineTask.Stop();
    }

    /// <summary>
    /// ��������, ����������� �������� � ����� �� ��������� ������������, ��� ������������
    /// ��� �������� �����, � � ������������
    /// </summary>
    /// <param name="coroutineTask"></param>
    /// <returns></returns>
    private IEnumerator PerformCheckingRandomObstacle(CoroutineTask coroutineTask)
    {
        CoroutineTask rotationCoroutineTask = new CoroutineTask();

        movingObject.SetEnabledAutomaticRotation(true);

        // ������� ����� �� �������������, ��� ��� ���������� �����
        if (FindPointToSearch(out ObstacleHidingPoint hidingPoint))
        {
            movingObject.MoveToPoint(hidingPoint.Point);

            yield return new WaitForFixedUpdate();
        }

        bool checkOpacityChange = true;
        bool pointReached = false;

        while (target != null)
        {
            while (hidingPoint != null)
            {
                if (movingObject.IsDestinationReached())
                {
                    pointReached = true;
                    break;
                }
                else if (checkOpacityChange && !hidingPoint.Obstacle.IsOpaque())
                {
                    // ���� ��� ��������� ����������� �� ��������� ������������
                    checkOpacityChange = false;
                    break;
                }
                yield return new WaitForFixedUpdate();
            }

            if (pointReached)
            {
                // �������� ����� ���������� �� ������������, ��������� �������
                rotationCoroutineTask.Start();
                StartCoroutine(PerformRotatingLeftRight(behindObstacleRotationAngle, movingObject.RotationSpeed, 
                    behindObstacleRotationTime, rotationCoroutineTask));
                while (rotationCoroutineTask.IsPerforming)
                {
                    yield return null;
                }

                break;
            }

            // ���� �� ����� ����� ��� ����������� ����� ����� ����������, �� ����� ���������
            // �� ������� �����
            movingObject.Stop();

            rotationCoroutineTask.Start();
            StartCoroutine(PerformRotating(noObstacleRotationAngle, movingObject.RotationSpeed, 
                rotationCoroutineTask));

            bool canContinueMoving = false;
            while (rotationCoroutineTask.IsPerforming)
            {
                yield return null;

                // ��������� �������, ���� ����������� ����� ����� �������
                if (hidingPoint != null && hidingPoint.Obstacle.IsOpaque())
                {
                    rotationCoroutineTask.Cancel();
                    canContinueMoving = true;
                    break;
                }
            }

            // ������� ����������, �� ����������� �� ����� ������� (��� ����� �� ���� �������),
            // ��������� �������� �� ������
            if (!canContinueMoving)
            {
                break;
            }

            // ����������� ����� �������, ���������� �������� � �����

            movingObject.MoveToPoint(hidingPoint.Point);

            yield return new WaitForFixedUpdate();
        }

        coroutineTask.Stop();
    }

    /// <summary>
    /// ��������, ������� ��������� ����� ������
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformMoving()
    {
        CoroutineTask coroutineTask = new CoroutineTask();

        // ������� ��� � �����, ��� ���� � ��������� ��� ����� ������
        coroutineTask.Start();
        StartCoroutine(PerformCheckingLastTargetPoint(coroutineTask));
        while (coroutineTask.IsPerforming)
        {
            yield return null;
        }

        // ���������� ����� ������ �� ������������ ������������
        coroutineTask.Start();
        StartCoroutine(PerformCheckingRandomObstacle(coroutineTask));
        while (coroutineTask.IsPerforming)
        {
            yield return null;
        }

        movingCoroutine = null;
    }

    /// <summary>
    /// �������� ����� ����� � �������������, ������� ��������� � ���� ������ �����
    /// </summary>
    /// <returns></returns>
    private List<ObstacleHidingPoint> GetVisibleHidinigPoints()
    {
        // �������� ��� ����� ������ ����������� �� ��������� �����������
        List<ObstacleHidingPoint> hidingPoints = ObstacleManager.Instance.HidingPoints.ToList();

        // ��������� �����, ������� ��������� ������ ���������� �����������
        for (int i = hidingPoints.Count - 1; i >= 0; i--)
        {
            if (!hidingPoints[i].Obstacle.IsOpaque())
            {
                hidingPoints.RemoveAt(i);
            }
        }

        List<ObstacleHidingPoint> visiblePoints = new List<ObstacleHidingPoint>();

        // ���� ������ ������� �����, ����������� � ������� ���� ������ �����
        foreach (var point in hidingPoints)
        {
            Vector3 vectorToPoint = point.Point - movingObject.Position;
            vectorToPoint.y = 0;
            if (Vector3.Dot(vectorToPoint, movingObject.transform.forward) >= 0 &&
                fov.IsPointInFovRadius(point.Point)
                && CanSeePoint(point.Point))
            {
                visiblePoints.Add(point);
            }
        }

        return visiblePoints;
    }

    /// <summary>
    /// �������� ������ ����������� �� ������ ����� ������
    /// </summary>
    /// <param name="hidingPoints"></param>
    /// <returns></returns>
    private List<IObstacle> GetObstaclesFromHidingPoints(List<ObstacleHidingPoint> hidingPoints)
    {        
        List<IObstacle> obstacles = new List<IObstacle>();

        foreach (var point in hidingPoints)
        {
            if (!obstacles.Contains(point.Obstacle))
            {
                obstacles.Add(point.Obstacle);
            }
        }

        return obstacles;
    }

    /// <summary>
    /// �������� ����� ������ �����������, ������� �������� ��� �����
    /// </summary>
    /// <param name="obstacles">������ �����������, ������ ������� ���� �����</param>
    /// <returns></returns>
    private List<ObstacleHidingPoint> GetInvisiblePointsAroundObstacles(List<IObstacle> obstacles)
    {
        // �������� ��� ����� ������ ����������� �� ��������� �����������
        List<ObstacleHidingPoint> allHidingPoints = ObstacleManager.Instance.HidingPoints.ToList();

        List<ObstacleHidingPoint> invisiblePoints = new List<ObstacleHidingPoint>();

        foreach (var point in allHidingPoints)
        {
            Vector3 vectorToPoint = point.Point - movingObject.Position;
            vectorToPoint.y = 0;
            if (obstacles.Contains(point.Obstacle) &&
                Vector3.Dot(vectorToPoint, movingObject.transform.forward) >= 0 &&
                Vector3.Dot(point.Normal, movingObject.transform.forward) >= 0 &&
                !CanSeePoint(point.Point))
            {
                invisiblePoints.Add(point);
            }
        }

        return invisiblePoints;
    }

    /// <summary>
    /// ����� ����� �� ������������ ������������, ��� ���� ����� �������� ����� ������
    /// </summary>
    /// <param name="pointToSearch"></param>
    /// <returns></returns>
    private bool FindPointToSearch(out ObstacleHidingPoint pointToSearch)
    {
        pointToSearch = null;

        // ����� �����������, ������� ����� ����
        List<ObstacleHidingPoint> visiblePoints = GetVisibleHidinigPoints();
        List<IObstacle> visibleObstacles = GetObstaclesFromHidingPoints(visiblePoints);

        // ����� ����� �� �������������, ������� � ������ ������ �� ����� �����
        List<ObstacleHidingPoint> invisiblePoints = GetInvisiblePointsAroundObstacles(visibleObstacles);

        // ������������ �������� �� �����. ��������� ������������ �����,
        // � ����� �����, ������������ �� ������� ����� ����� �������
        List<HidingPointPath> hidingPointPathList = GetPathToPointsList(invisiblePoints);

        if (TryChoosePath(hidingPointPathList, out HidingPointPath chosenPath))
        {
            pointToSearch = chosenPath.point;
            currentHidingPoint = pointToSearch;
            return true;
        }
        return false;
    }

    /// <summary>
    /// ��������� ������ ��������� � ������ ������ �����������.
    /// ����� ����� ���������� �������� �� ����������� ���������� ����� � ����� �����, 
    /// ����� ���� �� ������� ������ �����������
    /// </summary>
    /// <param name="hidingPoints">������ ����� ������ �����������</param>
    private List<HidingPointPath> GetPathToPointsList(List<ObstacleHidingPoint> hidingPoints)
    {
        List<HidingPointPath> pathList = new List<HidingPointPath>();

        foreach (var point in hidingPoints)
        {
            if (movingObject.CalculatePathToPoint(point.Point, out Vector3[] pathPoints))
            {
                float pathLength = VectorFunc.GetPathLength(pathPoints);
                // ������ ����� � ������� ������ ���� � ��, ����� ���� �� ������� ������ �����������
                if (pathLength <= 0 || (pathLength / movingObject.CurrentDesiredSpeed > maxObstacleSearchDuration))
                {
                    continue;
                }

                pathList.Add(new HidingPointPath(point, pathLength));            
            }
        }

        return pathList;
    }

    private bool TryChoosePath(List<HidingPointPath> pathList, out HidingPointPath chosenPath)
    {
        if (pathList.Count == 0)
        {
            chosenPath = default;
            return false;
        }

        // ������� ������ ����������� �� ��������� ��������� ��������
        List<IObstacle> obstacles = new List<IObstacle>();
        foreach (var path in pathList)
        {
            if (!obstacles.Contains(path.point.Obstacle))
            {
                obstacles.Add(path.point.Obstacle);
            }
        }

        // �� ����� ������ ����� ������� ������ �� ����� ��������� � ����� �����
        // ������� �����������
        List<HidingPointPath> minPathList = new List<HidingPointPath>();

        foreach (var obstacle in obstacles)
        {
            float minLength = float.MaxValue;
            HidingPointPath minPath = default;
            foreach (var path in pathList)
            {
                if (path.point.Obstacle == obstacle && path.pathLength < minLength)
                {
                    minLength = path.pathLength;
                    minPath = path;
                }
            }
            minPathList.Add(minPath);
        }

        // ��� �������. ����� �������� ����� ��������� ����������� � ����������� �����������,
        // �� ������� ���� �������� �����, ���� �����
        currentRetreatPathList = minPathList;

        // ��������� ����� ����� ��� ������������� ������������.
        // ��� ������� ���� �� �����, ��� ������ ����� �����������, ��� ���� ���� ������
        float weightSum = 0;
        foreach (var path in minPathList)
        {
            weightSum += 1 / path.pathLength;
        }

        // ���� ��������� ����� � ������������� [0,1)
        double randomProbabilityValue = random.NextDouble();
        
        // ������� �������� ���������������� �����������, � ������� ������
        // �������� �����
        chosenPath = minPathList[0];
        float probabilityRangeValue = 0;
        foreach (var path in minPathList)
        {
            float weight = 1 / path.pathLength;
            probabilityRangeValue += weight * 1 / weightSum;
            if (randomProbabilityValue < probabilityRangeValue)
            {
                chosenPath = path;
                break;
            }
        }

        return true;
    }

    /// <summary>
    /// ���������, ����� �� �����
    /// </summary>
    /// <param name="point">����������� �����</param>
    /// <returns></returns>
    private bool CanSeePoint(Vector3 point)
    {
        Vector3 vectorToPoint = point - fov.transform.position;
        vectorToPoint.y = 0;
        float distance = vectorToPoint.magnitude;

        return !AreObstaclesOnWay(fov.transform.position, vectorToPoint, distance);
    }

    /// <summary>
    /// ���������, ���� �� ����������� �� �������
    /// </summary>
    /// <param name="startPoint">��������� �����</param>
    /// <param name="direciton">����������� �������</param>
    /// <param name="distance">����� �������</param>
    /// <returns></returns>
    private bool AreObstaclesOnWay(Vector3 startPoint, Vector3 direciton, float distance)
    {
        return Physics.Raycast(startPoint, direciton, distance,
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore);
    }

    /// <summary>
    /// ���������� ��������� �� ������ �����
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        if (ObstacleManager.Instance == null || !ObstacleManager.Instance.DrawHidingPoints)
        {
            return;
        }

        // ����������� ������� ������� �����, ��������� ��� ����, ����� ����������
        if (currentHidingPoint != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawCube(currentHidingPoint.Point, new Vector3(1, 1, 1));
        }

        // ������������ ������ ��������� �����, �� ������� ���� �������� �����������
        if (currentRetreatPathList != null)
        {
            foreach (var item in currentRetreatPathList)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawCube(item.point.Point, new Vector3(0.7f, 0.7f, 0.7f));
            }
        }
    }

}
