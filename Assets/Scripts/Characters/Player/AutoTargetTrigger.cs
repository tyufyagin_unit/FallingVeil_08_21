using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// �������, � �������� �������� ���������� ������������� ������ ������ �� ������
/// (�������� � ������ �������� ��������)
/// </summary>
public class AutoTargetTrigger : MonoBehaviour
{   
    [SerializeField]
    [Tooltip("���������� �����, � ������� ���������� ���������� �� �������������")]
    private PlayerInput playerInput;

    [SerializeField]
    [Tooltip("���������� ������ ������")]
    private WeaponController weaponController;

    /// <summary>
    /// ��������� �������������
    /// </summary>
    private BoxCollider autoTargetCollider;

    /// <summary>
    /// ����, �� ������� ��������� �����
    /// </summary>
    private LayerMask enemyLayers;

    private void Awake()
    {
        autoTargetCollider = GetComponent<BoxCollider>();

        transform.position = weaponController.AttackPoint.position +
            weaponController.AttackPoint.forward * weaponController.AttackDistance / 2;

        Vector3 size = autoTargetCollider.size;
        size.z = weaponController.AttackDistance / transform.lossyScale.z;
        size.x = playerInput.AutoTargetingRadius;
        autoTargetCollider.size = size;
    }

    private void Start()
    {
        enemyLayers = GameManager.Instance.EnemyLayers;
    }

    private void OnTriggerStay(Collider other)
    {
        // ������������� ����� ��������, ������ ���� ����� ��� ����� �����, �� ���� ����� ���
        if (!weaponController.IsHitPerforming() || weaponController.GetTarget() != null)
        {
            return;
        }

        // ��������, ��� ������ ��������� �� ���� ������
        if (!other.gameObject.IsLayerInLayerMask(enemyLayers))
        {
            return;
        }

        Enemy enemy = other.GetComponent<Enemy>();

        if (enemy == null)
        {
            return;
        }

        // ��������, ��� ��������� �������� ��� �����, � ��� �� ���� ����� ��� �����������
        if (weaponController.CheckTargetAttackDistance(enemy) != AttackCheckResult.Ok ||
            weaponController.ThereAreObstaclesOnAttackWay(enemy))
        {
            return;
        }
        
        // ������������� ���� ��� ������
        weaponController.SetTarget(enemy);
    }

}
