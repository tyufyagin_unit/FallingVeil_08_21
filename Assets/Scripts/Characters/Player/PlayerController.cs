using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ���������� ������. ��������� ������� �������� � �������� ������, ���������� �� PlayerInput.
/// ��������� ������ ����� �� ���� ���������� � ������� ��������, ���������� � �������� ������.
/// ����������� �� ������� ������ ������.
/// �������, ����� �� ������ ������ ��� �������� Rigidbody � ���������.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class PlayerController : MovingObject
{    
    /// <summary>
    /// ��������� ���������. ��������, ������� ����� ������� ��� �������� �������� �������� ������
    /// � ���� ��� ������� � RigidBody
    /// </summary>
    private const float speedToForceConvertShift = 0.5f;

    [SerializeField]
    [Tooltip("������������ ���� ����� �������� �������� �������� � �������� �������� �����������, " +
        "��� ������� �������� ����� ������� �����������")]
    private float maxMoveArountAngle = 80;

    /// <summary>
    /// Rigidbody ������
    /// </summary>
    private Rigidbody playerRigidbody;

    /// <summary>
    /// ����, � ������� �������������� ����������� �� Rigidbody ��� ��������
    /// </summary>
    private float playerForce;

    /// <summary>
    /// ������� �������� �������� ������������ ��������
    /// </summary>
    private Quaternion targetRotation;

    /// <summary>
    /// ������� �������� ������
    /// </summary>
    public override Vector3 CurrentVelocity => playerRigidbody.velocity;

    protected override void Awake()
    {
        base.Awake();
        playerRigidbody = GetComponent<Rigidbody>();

        // ������������ �������� � ����
        playerForce = (speed + speedToForceConvertShift) * playerRigidbody.mass * playerRigidbody.drag;
    }

    /// <summary>
    /// �������� ������ �� ���� X, Z
    /// </summary>
    /// <param name="movementDirection">������� ����������� ��������</param>
    public override void Move(Vector3 movementDirection)
    {
        MoveWithObstaclesTest(movementDirection);
    }

    /// <summary>
    /// ��������� ������� ������, ����� ����� ��������� ����� ������ ����� � �����
    /// </summary>
    /// <param name="lookAtPoint">�����, � ������� ����� �����������</param>
    public virtual void RotateSmooth(Vector3 lookAtPoint)
    {
        lookAtPoint.y = transform.position.y;
        targetRotation = Quaternion.LookRotation(lookAtPoint - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation,
            Time.fixedDeltaTime * rotationSpeed);
    }

    /// <summary>
    /// �������� ������� ����������� ��������: ������ ��� �����
    /// </summary>
    /// <returns>1 - ������� �� ������� �������, -1 - ������, 0 - �������� �� ���������</returns>
    public override int GetTurnDirection()
    {
        const float minRotationDifferenceForTurn = 1;

        // ���� ������� �������� �������� ��������� � �������, ����������, � ����� ����������� �� ����� ��������������
        float angle1 = transform.rotation.eulerAngles.y;
        float angle2 = targetRotation.eulerAngles.y;
        float difference = angle2 - angle1;

        float differenceAbs = Mathf.Abs(difference);
        // �� ����� ������������ �������, ���� ������� ����� ������ ����� ����.
        if (differenceAbs <= minRotationDifferenceForTurn)
        {
            return 0;
        }

        // ���� ������� �������������, ������ ������� ������� ������ ��������, � �� �������������� �� ������� �������,
        // � ��������� ������ - ������ �������.
        // ������, ���� ������� ����� ������ ������ 180, �� ����� ������������� 
        if (differenceAbs > 180)
        {
            difference += difference > 0 ? -360 : 360;
        }

        return difference > 0 ? 1 : -1;
    }

    /// <summary>
    /// ���������� �������� ������ ������ �����������.
    /// </summary>
    /// <param name="movementDirection">�������� ����������� ��������</param>
    /// <param name="moveAroundVector">������������ ������ ������ �����������</param>
    /// <returns>true - ������ ������, false - �� ���� ��� �����������</returns>
    private bool TryGetMoveAroundVector(Vector3 movementDirection, out Vector3 moveAroundVector)
    {
        // ���������, ���� �� ����������� �� ���� �������� �� ������� movementDirection
        if (playerRigidbody.SweepTest(movementDirection, out RaycastHit hit, 1, QueryTriggerInteraction.Ignore))
        {
            // ���� ���� �����������, ��� ����� ������ ����� ��� ������. ��� ����� �������� ������� � �����������
            // �����������. ������ ������ - ��� ��������� �� 90 �������� ������ �������.
            // �� ���� ��������� �������� (����� ��� ������) ���� ������, ������� ����� ������� ����
            // � �������� ��������� ����������� ��������.

            Vector3 normal = hit.normal;
            normal.y = 0;

            moveAroundVector = VectorFunc.GetRotatedUnitVector(normal, 90);

            if (Vector3.Angle(movementDirection, -moveAroundVector)
                < Vector3.Angle(movementDirection, moveAroundVector))
            {
                moveAroundVector = -moveAroundVector;
            }

            return true;
        }

        moveAroundVector = Vector3.zero;
        return false;
    }

    /// <summary>
    /// ��������� �������� � ��������� �����������, ����������� �� ����
    /// </summary>
    /// <param name="movementDirection">������� ����������� ��������</param>
    private void MoveWithObstaclesTest(Vector3 movementDirection)
    {
        // ��� ������ �������� �����������, ���� �� ���������
        if (movementDirection == Vector3.zero)
        {
            return;
        }

        Vector3 newDirection = movementDirection;

        // ��������, ���� �� �� ���� ������� ��������� �������� �����������. ���� ����, ����� ����������������� ������
        // ��������, ��������� �����������.
        if (TryGetMoveAroundVector(movementDirection, out Vector3 moveAroundVector))
        {
            // �� ����� ���������, ���� ���� ����� �������� ��������� �������� � �������� �������� ������� �������
            if (Vector3.Angle(movementDirection, moveAroundVector) > maxMoveArountAngle)
            {
                return;
            }

            // �� ����� ���������, ���� ��� �������� � ����� ����������� ����� ��������� � �����������
            if (TryGetMoveAroundVector(moveAroundVector, out _))
            {
                return;
            }

            newDirection = moveAroundVector;
        }

        playerRigidbody.AddForce(newDirection * playerForce);
    }
}
