﻿using UnityEngine;

/// <summary>
/// Абстрактный класс, отвечающий за выполнение движения и поворота некоторого игрового объекта.
/// От него наследуются классы, которые обрабатывают команды движения игровых объектов 
/// определнного типа (отдельный класс для движения врагов, отдельный класс для движения игрока)
/// Добавляется на объект, который будет выполнять движение (ировой объект врага, игрока)
/// </summary>
public abstract class MovingObject : MonoBehaviour, ISaveable
{
    [SerializeField] 
    [Tooltip("Скорость движения")]
    protected float speed = 3;

    [SerializeField]
    [Tooltip("Скорость поворота")]
    protected float rotationSpeed = 60;

    /// <summary>
    /// Начальное состояние Transform объекта, которое было при загрузке сцены
    /// </summary>
    protected TransformData initialTransformData;

    /// <summary>
    /// Максимально возможная скорость движения объекта
    /// </summary>
    public float MaxSpeed { get; protected set; }

    /// <summary>
    /// Скорость поворота вокруг оси Y
    /// </summary>
    public float RotationSpeed => rotationSpeed;

    /// <summary>
    /// Текущая позиция
    /// </summary>
    public virtual Vector3 Position => transform.position;

    /// <summary>
    /// Текущая скорость движения, которую пытается достичь объект
    /// (т.е. текущая максимальная скорость движения) 
    /// </summary>
    public virtual float CurrentDesiredSpeed => speed;

    /// <summary>
    /// Текущая скорость движения
    /// (переопределяется в наследниках)
    /// </summary>
    public abstract Vector3 CurrentVelocity { get; }

    /// <summary>
    /// Выполнить движение по вектору движения 
    /// (переопределяется в наследниках)
    /// </summary>
    /// <param name="movementVector">Вектор движения</param>
    public virtual void Move(Vector3 movementVector)
    {
    }

    /// <summary>
    /// Выполнить движение к точке 
    /// (переопределяется в наследниках)
    /// </summary>
    public virtual void MoveToPoint(Vector3 point)
    {
    }

    /// <summary>
    /// Выполнить мгновенный поворот так, чтобы стоять лицом к точке
    /// </summary>
    /// <param name="lookAtPoint">Точка, к которой надо повернуться</param>
    public virtual void Rotate(Vector3 lookAtPoint)
    {
        lookAtPoint.y = transform.position.y;
        transform.LookAt(lookAtPoint);
    }

    /// <summary>
    /// Выполнить мгновенный поворот на определенный градус вокруг оси Y
    /// </summary>
    /// <param name="rotation">Угол, на который нужно повернуться</param>
    public virtual void Rotate(float rotation)
    {
        transform.Rotate(new Vector3(0, rotation, 0));
    }

    /// <summary>
    /// Получить текущее направление поворота
    /// (переопределяется в наследниках)
    /// </summary>
    /// <returns>1 - вправо, -1 - влево, 0 - поворот не выполняется</returns>
    public virtual int GetTurnDirection()
    {
        return 0;
    }

    /// <summary>
    /// Задать состояние объекта. 
    /// Используется для восстановления состояния объекта при возрождении на контрольной точке.
    /// Потенциально метод может быть использован для загрузки из файла,
    /// но сейчас мы используем его только для того, чтобы при начале игры с контрольной точки
    /// восстановить первоначальное состояние объекта
    /// </summary>
    /// <param name="state"></param>
    public virtual void SetState(object state)
    {
        // Восстанавливаем исходное положение объекта, которое было при первоначальной загрузке сцены
        transform.SetPositionAndRotation(initialTransformData.position, initialTransformData.rotation);
    }

    /// <summary>
    /// Получить состояние объекта.
    /// Потенциально метод может быть использован для сохранения объекта в файл,
    /// сейчас это не требуется, поэтому возвращаем пустую строку в качестве состояния
    /// </summary>
    /// <returns></returns>
    public virtual object GetState()
    {
        return "";
    }

    /// <summary>
    /// Получить расстояние от объекта ИИ до точки
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public float GetDistanceToPoint(Vector3 point)
    {
        return VectorFunc.GetDistanceXZ(Position, point);
    }

    /// <summary>
    /// Получить квадрат расстояния от объекта ИИ до точки
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public float GetSqrDistanceToPoint(Vector3 point)
    {
        return VectorFunc.GetSqrDistanceXZ(Position, point);
    }

    protected virtual void Awake()
    {
        MaxSpeed = speed;      

        initialTransformData = new TransformData
        {
            position = transform.position,
            rotation = transform.rotation
        };
    }
}
