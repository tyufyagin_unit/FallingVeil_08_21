﻿/// <summary>
/// Структура, хранящая параметры маршрута к точке укрытия за каким-либо препятствием
/// </summary>
public struct HidingPointPath
{
    /// <summary>
    /// Точка укрытия
    /// </summary>
    public ObstacleHidingPoint point;
    /// <summary>
    /// Длина маршрута до точки
    /// </summary>
    public float pathLength;

    public HidingPointPath(ObstacleHidingPoint point, float pathLength)
    {
        this.point = point;
        this.pathLength = pathLength;
    }
}
