﻿using System.Collections;
using UnityEngine;
using System;
using UnityEngine.VFX;
using System.Linq;

/// <summary>
/// Класс, использующий визуальный эффект для отображения атакующего луча
/// </summary>
public class AttackBeamVFX : AttackBeamRenderer
{
    [SerializeField]
    [Tooltip("Визуальный эффект VFX Graph")]
    private VisualEffect visualEffect;

    /// <summary>
    /// Записываем текущую частоту мерцания
    /// </summary>
    private float currentFrequency = 0;

    /// <summary>
    /// Запущена ли корутина мерцания
    /// </summary>
    private bool isFlickering = false;

    /// <summary>
    /// Корутина мерцания
    /// </summary>
    private Coroutine flicker = null;

    /// <summary>
    /// Установить начальную и конечную точку луча и частоту мигания луча
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="endPoint"></param>
    public override void SetPosition(Vector3 startPoint, Vector3 endPoint)
    {
        transform.position = startPoint;
        transform.LookAt(endPoint);
        visualEffect.SetFloat("Distance", Vector3.Distance(startPoint, endPoint));

        if (beam.IsAttacking() && beam.GetTarget() != null)
        {
            for (int i = 0; i < indentAndFrequency.Length; i++)
            {
                //если это первый элемент в массиве, тогда верхняя граница - это максимальная длина (которая всегда соблюдается)
                float maxBorder = beam.MaxDistance;

                //если это не первый элемент массива, то верхняя граница длины луча - это значение i - 1 (предыдущего) элемента массива
                if ((indentAndFrequency.Length > 1) && (i > 0)) maxBorder = beam.MaxDistance - indentAndFrequency[i - 1].x;

                if ((Vector3.Distance(startPoint, endPoint) > beam.MaxDistance - indentAndFrequency[i].x) && (Vector3.Distance(startPoint, endPoint) < maxBorder))
                {
                    if (!isFlickering)
                    {
                        flicker = StartCoroutine(BeamFlicker());
                        isFlickering = true;
                    }

                    if (currentFrequency > 0) currentFrequency = 1 / indentAndFrequency[i].y;
                    else currentFrequency = 1;
                }
            }
            if (Vector3.Distance(startPoint, endPoint) < (beam.MaxDistance - indentAndFrequency[indentAndFrequency.Length - 1].x))
            {
                this.StopAndNullCoroutine(ref flicker);

                isFlickering = false;
                visualEffect.SetFloat("Alpha", 1f);
            }
        }
    }

    private void OnDisable()
    {
        isFlickering = false;
        this.StopAndNullCoroutine(ref flicker);
        visualEffect.SetFloat("Alpha", 1f);
    }

    private IEnumerator BeamFlicker()
    {
        while (true)
        {
            {
                yield return new WaitForSeconds(currentFrequency);
                visualEffect.SetFloat("Alpha", 0f);
                yield return new WaitForSeconds(0.3f);
                visualEffect.SetFloat("Alpha", 1f);
            }
        }

    }
}
    
