﻿using UnityEngine;

/// <summary>
/// Абстракнтый базовый класс для обработки события изменения уровня видимости объектов.
/// Служит для реализации определённого поведения некоторого объекта при получении
/// события изменения уровня видимости.
/// </summary>
public abstract class OpacityChangingHandler : MonoBehaviour
{
    /// <summary>
    /// Контроллер уровня видимости объектов
    /// </summary>
    protected OpacityController opacityController = null;

    /// <summary>
    /// Предыдущее значение видимости объектов
    /// </summary>
    protected float previousOpacityValue = 0;

    protected void Start()
    {
        opacityController = GameManager.Instance.Controller.OpacityController;
        OnEnable();
    }

    protected void OnEnable()
    {
        if (opacityController != null)
        {
            opacityController.RegisterOpacityChangedListener(OnOpacityChanged);
            previousOpacityValue = opacityController.OpacityValue;
        }
    }

    protected void OnDisable()
    {
        if (opacityController != null)
        {
            opacityController.UnregisterOpacityChangedListener(OnOpacityChanged);
        }
    }

    /// <summary>
    /// Обработчик события изменения видимости объектов
    /// </summary>
    protected abstract void OnOpacityChanged();
}
